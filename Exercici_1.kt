import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    val mida = scanner.nextInt()

    //LISTAS
    val secuenciaList = mutableListOf<Int>()
    val minList = mutableListOf<Int>()
    val maxList = mutableListOf<Int>()

    //Introducción de valores de entrada a lista.
    println("Introduzca los valores: ")
    for (i in 0 until mida){
        val valores = scanner.nextInt()
        secuenciaList.add(valores)
    }

    var media = 0
    //Media de todos los valores de la secuencia.
    for (i in 0 until secuenciaList.lastIndex){
        media += i
    }

    println("Secuencia introducida: $secuenciaList")
    println("Mínimo: ${secuenciaList.min()}")
    println("Máximo: ${secuenciaList.max()}")
    println("Media: ${secuenciaList.average()}")

}