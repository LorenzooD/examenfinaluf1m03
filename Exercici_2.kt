import java.util.*



/*
Planteamiento:
Lo primero que he hecho es crear 3 listas mutables donde se almacenarán las 3 listas.
Despues he añadido un contador para cada lista en la que mediante un for se vaya comparando los valores, y si el valor i es menor que j se sume
 */
fun main(){
    val scanner = Scanner(System.`in`)
    val nSecuencias = scanner.nextInt()

    //LISTAS
    val secuenciaList1 = mutableListOf<Int>()
    val secuenciaList2 = mutableListOf<Int>()
    val secuenciaList3 = mutableListOf<Int>()

    var count1 = 0
    var count2 = 0
    var count3 = 0

    //PETICIÓN
    //Primera secuencia
    do {
        val valores = scanner.nextInt()
        secuenciaList1.add(valores)
        for (i in 0 until secuenciaList1.lastIndex){
            for (j in 1 until secuenciaList1.lastIndex){
                if (i < j) count1++
            }
        }
    }while (valores != 0)

    //Segunda secuencia
    do {
        val valores = scanner.nextInt()
        secuenciaList2.add(valores)
        for (i in 0 until secuenciaList1.lastIndex){
            for (j in 0 until secuenciaList1.lastIndex){
                if (i < j)count2++ //CORREGIR ERROR. SUMA TODOS LOS VALORES DE LA LISTA
            }
        }
    }while (valores != 0)

    //Tercera secuencia
    do {
        val valores = scanner.nextInt()
        secuenciaList3.add(valores)
        for (i in 0 until secuenciaList1.lastIndex){
            for (j in 0 until secuenciaList1.lastIndex){
                if (i < j)count3++ //CORREGIR ERROR. SUMA TODOS LOS VALORES DE LA LISTA
            }
        }
    }while (valores != 0)


    println("Sentencia 1: $count1")
    println("Sentencia 2: $count2")
    println("Sentencia 3: $count3")

}