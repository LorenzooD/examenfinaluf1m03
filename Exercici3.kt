import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)

    //LISTA
    val palabrasRepetidas = mutableListOf<String>()

    //LOGICA
    while (true) {
//-------TURNOS-------//

        //J2
        println("Turno del jugador 1: ")
        val j1 = scanner.next()

        //j2
        println("Turno del jugador 2: ")
        val j2 = scanner.next()

//-------COMPROBACIÓN DE PRIMERA Y ÚLTIMA LETRA-------//
        //Si la primera letra del jugador 2 no es igual a la última del jugador 1, J1 gana
        if (j2.first() != j1.last()) {
            println("¡EL JUGADOR 1 HA GANADO!")
            break
        }
        //Si la primera letra del jugador 1 no es igual a la última del jugador2, J2 gana
        if (j1.last() != j2.first()) {
            println("¡EL JUGADOR 2 HA GANADO!")
            break
        }
//-------COMPROBACIÓN DE PALABRAS REPETIDAS-------//

        //Comprobación de que la palabra introducida por el jugador 1 no está en la lista. En caso contrario lo añade
        if (j1 !in palabrasRepetidas){
            palabrasRepetidas.add(j1)
        }else println("¡EL JUGADOR 2 HA GANADO!")

        //Comprobación de que la palabra introducida por el jugador 2 no está en la lista. En caso contrario lo añade
        if (j2 !in palabrasRepetidas){
            palabrasRepetidas.add(j2)
        }else println("¡EL JUGADOR 1 HA GANADO!")

    }

}