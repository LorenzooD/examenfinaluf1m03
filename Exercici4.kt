import java.util.*

/*
* AUTHOR: Lorenzo Poderoso Dalmau
* DATE: 29/11/2022
* TITLE: Files i columnes
*/

fun main(){
    //Matriz
    val matrix: MutableList<MutableList<Int>> = mutableListOf()
    val resultColumna = mutableListOf<List<Int>>()
    val diagonalMatrix = mutableListOf<Int>()

    //ENTRADA
    val scanner = Scanner(System.`in`)
    println("Introduzca las columnas de la matriz: ")
    val columnas = scanner.nextInt()
    println("Introduzca las filas de la matríz: ")
    val filas = scanner.nextInt()


    //-----ASIGNACIÓN RANDOM DE VALORES PARA LA MATRÍZ Y OUTPUT-----//
    println("Los valores de su matriz son de $columnas x $filas ")
    for (i in 0 until columnas){
        matrix.add(mutableListOf())
        for (j in 0 until filas){
            var randomNum = (-30..30).random()
            matrix[i].add(randomNum)
        }
    }
    //Output de matríz
    for (i in 0 until matrix.size){
        println(matrix[i])
    }

    //Logica

        for (i in 0 until matrix.size){
            for (j in 0 until matrix.size){
                if (columnas == filas)
                diagonalMatrix += matrix[i][j]
                break
        }
    }
    var sum = 0
    for (i in 0 until diagonalMatrix.size){
        for (j in 0 until diagonalMatrix.size){
            sum += i+j
        }
    }

    println("Resultado $sum")

}
